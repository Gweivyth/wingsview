# Wingsview

A newer, cleaner WingsXI.com.

This project will remain open source to comply with the WingsXI AGPL license agreement.  Read more here: https://gitlab.com/ffxiwings/wings/-/blob/master/LICENSE
